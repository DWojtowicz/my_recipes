#!/bin/bash

# Deploys providers on the machines reserved under n°$1

oargridstat -w -l "$1" | sed '/^$/d' | sort -u > ~/public/machines
cp ~/public/machines ~/public/machines_1
kadeploy3 provider -M -f ~/public/machines -p TMP
