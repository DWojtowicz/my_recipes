# Images configuration

## `provider`

Environment with PostgreSQL readily set up with the IMDb dataset and a simulated DBaaS provider.

## `nebula`

Environment with Nebula readily set up.
