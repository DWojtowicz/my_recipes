#!/bin/bash

export PROVIDER_NAME=${HOSTNAME%%.*}
export PROVIDER_NAME_SQL=${HOSTNAME%%-*}

export ALL_RAM=$(expr $(grep MemTotal /proc/meminfo | awk '{print $2}') / 1024)

if [ ! -f /root/tuning_ok ]
then
	export PGCONFFILE=/etc/postgresql/14/main/postgresql.conf
	# Memory tuning
	echo "work_mem = '$(expr $ALL_RAM / 4)MB'" >> $PGCONFFILE
	echo "maintenance_work_mem = '$(expr $ALL_RAM / 20)MB'" >> $PGCONFFILE
	echo "effective_cache_size = '$(expr $ALL_RAM / 2)MB'" >> $PGCONFFILE

	# SSD tuning
	if lsblk -o rota,mountpoint | grep -q '0 /' ; then
		echo "random_page_cost = 1.1" >> $PGCONFFILE
	fi
	# systemctl restart postgresql
	systemctl start postgresql.service
	echo "PostgreSQL tuned!"
	touch /root/tuning_ok
fi

echo "Installing the provider"
cd /root

if [ -d /root/nebula ]
then
	rm -R /root/nebula
fi


# Downloading the provider
git clone https://framagit.org/DWojtowicz/nebula.git

# Setting up the provider
sed -i 's/xxxPROV_NAMExxx/'"$PROVIDER_NAME"'/g' /root/nebula/nebula/provider/conf_template.json
sed -i 's/xxxHOST_URLxxx/'"$HOSTNAME"'/g' /root/nebula/nebula/provider/conf_template.json

if [ ! -f /root/db_setup_ok ]
then
	# Renaming attributes and tables to match current provider
	wget --no-cache http://public.grenoble.grid5000.fr/~dwojtowicz/at_image_boot/rename.sql -O /root/rename.sql
	wget --no-cache http://public.grenoble.grid5000.fr/~dwojtowicz/at_image_boot/rename.sql -O /root/undo_rename.sql
	sed -i 's/xxxPROV_NAMExxx/'"$PROVIDER_NAME_SQL"'/g' /root/rename.sql
	sed -i 's/xxxPROV_NAMExxx/'"$PROVIDER_NAME_SQL"'/g' /root/undo_rename.sql
	psql -U postgres -w -d imdbload -f /root/rename.sql

	# Creating the object_size function in the DB required by the provider
	wget --no-cache http://public.grenoble.grid5000.fr/~dwojtowicz/at_image_boot/object_size_function.sql -O /root/object_size_function.sql
	psql -U postgres -w -d imdbload -f /root/object_size_function.sql

	touch /root/db_setup_ok
fi

# Getting ready to run the provider
cd /root/nebula
chmod 777 ./*.sh
bash ./setup_env.sh
bash ./provider.sh /root/nebula/nebula/provider/conf_template.json
