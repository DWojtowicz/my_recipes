#!/bin/bash

export PROVIDER_NAME=${HOSTNAME%%.*}
export PROVIDER_NAME_SQL=${PROVIDER_NAME//-/_}
wget http://public.grenoble.grid5000.fr/~dwojtowicz/nebula_instances
mv nebula_instances /root/nebula_instances

# Getting the number for the providers list
export MACHINES_NUMBER=$(sort -u /root/nebula_instances | grep -n $HOSTNAME | sed 's/^\([0-9]\+\):.*$/\1/')
echo $MACHINES_NUMBER
wget http://public.grenoble.grid5000.fr/~dwojtowicz/machines_$MACHINES_NUMBER
mv machines_$MACHINES_NUMBER /root/machines

# Cleaning nebula if it exists
if [ -d /root/nebula ]
then
	rm -R /root/nebula
fi

cd /root

# Downloading the provider
git clone https://framagit.org/DWojtowicz/nebula

# Getting ready to run the provider
cd /root/nebula
chmod 777 ./*.sh
bash setup_env.sh
bash nebula_g5k_localfile.sh /root/machines
