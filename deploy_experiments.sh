#!/bin/bash

# $1 : Providers' job id
# $2 : Nebula's job id

if [ "$#" -ne 2 ]; then
	echo "Usage: ./deploy_experiments.sh PROVIDERS_JOB_ID NEBULAS_JOB_ID"
	exit
fi

export FICHIERMACHINES=~/public/machines
export FICHIERNEBULAINSTANCES=~/public/nebula_instances

# Seting up the machines list files

oargridstat -w -l "$1" | sed '/^$/d' | sort -u > $FICHIERMACHINES
oargridstat -w -l "$2" | sed '/^$/d' | sort -u > $FICHIERNEBULAINSTANCES
export NBEXPERIMENTS=$(wc -l < $FICHIERNEBULAINSTANCES)

echo "Deploying for "$NBEXPERIMENTS" instances of Nebula"

for i in $(seq 1 $NBEXPERIMENTS)
do
	rm $FICHIERMACHINES"_"$i
        touch $FICHIERMACHINES"_"$i
done

# Populating providers list
CPT=0
cat $FICHIERMACHINES | while read l
do
	# echo $CPT $l $(($CPT % $1 + 1))
	echo $l >> $FICHIERMACHINES"_"$(($CPT % $NBEXPERIMENTS + 1))
	let CPT=CPT+1
done

# Running the providers and Nebula

kadeploy3 provider -M -f $FICHIERMACHINES -p TMP &
kadeploy3 nebula -M -f $FICHIERNEBULAINSTANCES &
wait
echo "Deployment done! Nebula available at the following addresses:"
cat $FICHIERNEBULAINSTANCES
